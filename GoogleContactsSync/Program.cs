﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.Contacts;
using Google.GData.Contacts;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.Apis.Auth.OAuth2;

namespace GoogleContactsSync {
	class Program {
		static void Main(string[] args) {

			OAuth2Parameters parameters = new OAuth2Parameters() {
				ClientId="140693368249-loj00tbr7cgoniahfrtcioqk56u00cb9.apps.googleusercontent.com",
				ClientSecret="X4xZNvOCaPF1EnOCAaNmzTcV",
				RedirectUri="urn:ietf:wg:oauth:2.0:oob",
				Scope="https://www.google.com/m8/feeds"
			};

			string url = OAuthUtil.CreateOAuth2AuthorizationUrl(parameters);
			Console.WriteLine("Authorize URI:\n"+url);
			Console.WriteLine("Enter auth token:");
			parameters.AccessCode=Console.ReadLine();

			OAuthUtil.GetAccessToken(parameters);

			RequestSettings requestSettings = new RequestSettings("mtela-contacts", parameters);
			//requestSettings.AutoPaging=true;
			ContactsRequest contactsRequest = new ContactsRequest(requestSettings);

			ContactsQuery query = new ContactsQuery(ContactsQuery.CreateContactsUri("default"));
			query.NumberToRetrieve=9999;
			query.ShowDeleted=false;

			Feed<Contact> feed = contactsRequest.Get<Contact>(query);
			//Feed<Contact> feed = contactsRequest.GetContacts();

			//Feed<Contact> feed = cr.Get<Contact>(query);
			//if(feed.Entries.Count()>0) {
			var i = 1;
				foreach(Contact contact in feed.Entries) {
					//Console.WriteLine(contact.Name.FullName);
					String contact_str = "\n\n\n"+i+". ID: "+contact.Id
					+"\nName: "+contact.Name.FullName;

					if(contact.Phonenumbers.Count > 0) {
						contact_str+="\nPhone numbers:";
						foreach(var phone in contact.Phonenumbers) {
							contact_str+="\n\t"+phone.Rel+": "+phone.Value;
						}
					}

					if(contact.Emails.Count>0) {
						contact_str+="\nEmails:";
						foreach(var email in contact.Emails) {
							contact_str+="\n\t"+email.Rel+": "+email.Address;
						}
					}

					//if(contact.Content != null)
					if(!string.IsNullOrWhiteSpace(contact.Content))
						contact_str+="\nNotes: "+contact.Content;

					contact_str+="\nUpdated on: "+contact.Updated.ToString();
					Console.WriteLine(contact_str);
					i++;
				}
			//} else {
			//	Console.WriteLine("No contacts");
			//}

			Console.WriteLine("That's all");
			Console.ReadKey();
		}

		public static void PrintAllContacts(ContactsRequest cr) {
			Feed<Contact> f = cr.GetContacts();
			Console.WriteLine("Number of contacts: "+f.Entries.GetType()+"\n\n");
			foreach(Contact entry in f.Entries) {
				Console.WriteLine(entry);
				if(entry.Name!=null) {
					Name name = entry.Name;
					if(!string.IsNullOrEmpty(name.FullName))
						Console.WriteLine("\t\t"+name.FullName);
					else
						Console.WriteLine("\t\t (no full name found)");
					if(!string.IsNullOrEmpty(name.NamePrefix))
						Console.WriteLine("\t\t"+name.NamePrefix);
					else
						Console.WriteLine("\t\t (no name prefix found)");
					if(!string.IsNullOrEmpty(name.GivenName)) {
						string givenNameToDisplay = name.GivenName;
						if(!string.IsNullOrEmpty(name.GivenNamePhonetics))
							givenNameToDisplay+=" ("+name.GivenNamePhonetics+")";
						Console.WriteLine("\t\t"+givenNameToDisplay);
					} else
						Console.WriteLine("\t\t (no given name found)");
					if(!string.IsNullOrEmpty(name.AdditionalName)) {
						string additionalNameToDisplay = name.AdditionalName;
						if(string.IsNullOrEmpty(name.AdditionalNamePhonetics))
							additionalNameToDisplay+=" ("+name.AdditionalNamePhonetics+")";
						Console.WriteLine("\t\t"+additionalNameToDisplay);
					} else
						Console.WriteLine("\t\t (no additional name found)");
					if(!string.IsNullOrEmpty(name.FamilyName)) {
						string familyNameToDisplay = name.FamilyName;
						if(!string.IsNullOrEmpty(name.FamilyNamePhonetics))
							familyNameToDisplay+=" ("+name.FamilyNamePhonetics+")";
						Console.WriteLine("\t\t"+familyNameToDisplay);
					} else
						Console.WriteLine("\t\t (no family name found)");
					if(!string.IsNullOrEmpty(name.NameSuffix))
						Console.WriteLine("\t\t"+name.NameSuffix);
					else
						Console.WriteLine("\t\t (no name suffix found)");
				} else
					Console.WriteLine("\t (no name found)");
				foreach(EMail email in entry.Emails) {
					Console.WriteLine("\t"+email.Address);
				}
			}
		}
	}
}
